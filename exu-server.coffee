import http from 'http'
import path from 'path'
import run from '@aurium/run'
import * as apps from './src/apps'
import apiRouter from './src/api'
import * as clientWS from './src/client-ws'
import { log, mkID, timeout, getExt, getMime, readOrCompileFile, urlDirname } from './src/helpers'
import { promises as fs } from 'fs'
import { initConfigDir, getExuConfig, MAIN_PORT } from './src/config'

shouldAutoInit = '--init' in process.argv.slice 2

dirname = `urlDirname(import.meta.url)`

exit = (args={})->
    {signal, exitStatus, message} = args
    log 'Server Shutdown!', args
    clientWS.broadcast 'server:shutdown', {signal, message}
    timeout 1, -> process.exit if exitStatus? then exitStatus else 1

export { exit }

if shouldAutoInit
    process.on 'SIGTERM', -> exit exitStatus: 0, signal: 'SIGTERM'
    process.on 'SIGINT',  -> exit exitStatus: 0, signal: 'SIGINT'
    process.on 'SIGHUP',  -> exit exitStatus: 0, signal: 'SIGHUP'
    initConfigDir()
    .then -> getExuConfig().set 'user.username', process.env.USER
    .then -> do apps.init
    .then -> do initServer
    .then (port)-> log "Exu WebDesktop listening at #{port}."
    .catch (err)->
        log err
        process.exit 1

initServer = -> new Promise (resolve, reject)->
    server = http.createServer (req, res)->
        return apiRouter(req, res) if req.url.match /^\/api([\/?].*)?$/
        reqPath = path.resolve req.url.split('?')[0]
        reqPath = '/index.html' if reqPath is '/'
        localPath = path.join dirname, 'public', reqPath
        log { localPath, dirname }
        Promise.all [
            readOrCompileFile localPath
            getMime localPath
        ]
        .then ([data, mime])->
            log "REQ \"#{reqPath}\" mime:", mime
            res.writeHead 200, 'Content-Type': mime
            res.end data
        .catch (err)->
            log "REQ \"#{reqPath}\" error:", err
            if err.code is 'ENOENT'
                res.writeHead 404, 'Content-Type': 'text/plain'
                res.end """There is no "#{reqPath}"."""
            else
                log err
                res.writeHead 500, 'Content-Type': 'text/html'
                res.end """
                <html>
                <head><title>(500) Internal Error</title></head>
                <body>
                    <h1>(500) Internal Error</h1>
                    <pre>#{err.message}</pre>
                </body></html>
                """
    clientWS.buildWSServer server
    server.listen MAIN_PORT, -> resolve MAIN_PORT
    server.on 'error', reject

