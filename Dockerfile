FROM node:14.3.0-buster

RUN ping deb.debian.org -c2
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install xserver-xorg xfwm4 lightdm net-tools

# The source will be mounted here:
RUN mkdir /opt/src
#...and then copyed tho here without host's node_modules:
WORKDIR /opt/app

COPY package.json .
RUN npm install

run echo "#!/bin/bash -e \n\
    echo -n 'Updating src into this container...' &&\n\
    cp -r \$(ls -1 /opt/src | grep -v node_modules | sed 's#^#/opt/src/#') ./ &&\n\
    echo -e '\rSource updated into this container.'" \
    > /usr/bin/update-src && chmod +x /usr/bin/update-src
