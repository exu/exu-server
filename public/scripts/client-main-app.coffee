import ExecWebUI from './ExecWebUI.js'

socket = io()

window.panic = (err)->
    console.error 'PANIC', err
    alert 'PANIC\n\n' + err.message

socket.emit 'fs:readdir', ['123', '/tmp']
socket.once "fs:resp:123", console.log

socket.emit 'apps:list', ['123']
socket.once "apps:resp:123", console.log

new ExecWebUI { termCollection, cmdInput }

shutdownBtn.addEventListener 'click', -> socket.emit 'shutdown'

### General Events ###

socket.on 'server:shutdown', ({signal, message})->
    console.log 'server:shutdown', {signal, message}
    alert 'ATTENTION!\n\nServer is shutdown now.\n' + message

socket.on 'askpass', ({askID, msg})->
    socket.emit "askpass:#{askID}", prompt(msg)

socket.on 'ERROR', (err)->
    panic { err..., message: 'Server Error: ' + err.message }

