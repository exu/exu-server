# Process an array in sequence with an async function.
export promiseChain = (array, func)->
    first = array[0]
    array = array[1..]
    chain = func first, 0
    array.forEach (item, i)-> chain = chain.then -> func item, i + 1
    return chain

export promiseTimeout = (secs)->
    new Promise (resolve)-> setTimeout resolve, secs * 1000

