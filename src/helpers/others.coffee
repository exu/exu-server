import log from '../logger'

export mkID = -> Math.random().toString(36).split('.')[1]

export parseParamsFromUrl = (url)-> exports.parseParams url.split('?')[1]

export parseParams = (params)->
    paramsObj = {}
    params.split('&').forEach (p)->
        p = p.split '='
        paramsObj[decodeURIComponent p[0]] = decodeURIComponent p[1]
    paramsObj

export timeout = (secs, fn)-> setTimeout fn, secs * 1000

class ExtendedError
    constructor: (err, funcName, extraData)->
        prefix = if funcName then funcName + ' fail: ' else ''
        this.message = "#{prefix}#{err.message}"
        this.stack = err.stack
        this.extraData = extraData if extraData?.length > 0

export safeFuncBuilder = (client, baseInfo...)-> (info..., fn)->
    funcName = info.shift()
    extraData = [baseInfo..., info...]
    (args...)->
        try
            fn.apply this, args
        catch err
            error = new ExtendedError err, funcName, extraData
            if client
                if client.emitError
                    client.emitError error
                else
                    log error
                    log new Error '''
                    ATTENTION!!! ATTENTION!!! ATTENTION!!! ATTENTION!!! ATTENTION!!!
                    Client not extended with `emitError`!'''
            else
                log error

