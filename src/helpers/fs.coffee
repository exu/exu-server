import run from '@aurium/run'
import sass from 'node-sass'
import { promises as fs } from 'fs'
import { log, mkID, promiseChain } from '.'

getExt = (path)-> path.replace /.*\./, ''

getMime = (path)->
    mime = {
        html: 'text/html'
        css:  'text/css'
        js:   'text/javascript'
    }[getExt path]
    if mime
        Promise.resolve mime
    else
        run('file', '--brief', '--mime-type', path).then (out)-> out.trim()

compileTo = {}

compileTo.js = (localPath)->
    src = localPath.replace /js$/, 'coffee'
    fs.stat src
    .then (stat)->
        run 'coffee', '--inline-map', '--print', src

compileTo.css = (localPath)->
    src = localPath.replace /css$/, 'scss'
    conf = file: src, sourceMapEmbed: on
    fs.stat src
    .then (stat)-> new Promise (resolve, reject)->
        sass.render conf, (err, result)->
            return reject err if err
            resolve result
    .catch (err)->
        src = localPath.replace /css$/, 'sass'
        conf = file: src, sourceMapEmbed: on, indentedSyntax: on
        fs.stat src
        .then (stat)-> new Promise (resolve, reject)->
            sass.render conf, (err, result)->
                return reject err if err
                resolve result.css

readOrCompileFile = (localPath)->
    fs.readFile localPath
    .catch (err)->
        throw err unless err.code is 'ENOENT'
        ext = getExt localPath
        if compileTo[ext]
            compileTo[ext] localPath
        else
            throw err

export { getExt, getMime, readOrCompileFile }
