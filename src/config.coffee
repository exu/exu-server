import path from 'path'
import { promises as fs } from 'fs'
import { log, mkID, promiseChain } from './helpers'

export HOST_NAME = process.env.HOST_NAME or 'localhost'
export MAIN_PORT = process.env.MAIN_PORT or 8000
export LANG = (process.env.LANG or process.env.LANGUAGE or 'en').split(/[:.]/)[0]

baseConfigDir = null
exuConfig = null

export getExuConfig = -> exuConfig

export initConfigDir = ->
    if baseConfigDir
        return log 'The config dir is already initialized.'
    baseConfigDir = process.env.CONFIG_DIR or path.join process.env.HOME, '.config/exu'
    baseConfigDir = path.resolve baseConfigDir
    log 'Exu config dir:', baseConfigDir
    dir = ''
    promiseChain baseConfigDir.split('/')[1..], (d)->
        step = dir += '/' + d
        fs.stat step
        .then (stat)->
            unless stat.isDirectory()
                throw Error "#{path} is not a directory."
        .catch (err)->
            if err.code is 'ENOENT'
                log 'Create dir:', step
                fs.mkdir step
            else
                throw err
        .catch (err)->
            log err
            process.exit 1
    .then -> configAccessor '__exu__'
    .then (conf)-> exuConfig = conf

getConfigDir = (name)->
    unless baseConfigDir
        throw Error '`initConfigDir()` was not called.'
    baseConfigDir + '/' + name

filterStep = (step, path)->
    if step[step.length-1] is ']'
        step = step.substr 0, step.length - 1
        stepInt = parseInt step
        if isNaN stepInt
            throw Error "[#{step}] is not a valid array index. (path: \"#{path}\")"
        step = stepInt
    step

class Config
    constructor: (@configDir, @configFile, @data)->

    # Save it to the config file inside the Exu config dir.
    save: ({returnVal})->
        log 'Save config', @configFile
        fs.writeFile @configFile, JSON.stringify @data, null, '  '
        .then -> returnVal

    getPath: (path)->
        node = @data
        pathIni = path.split(/\.|\[/).map (step)-> step = filterStep step, path
        # For each step, but the last:
        for step, i in pathIni[..pathIni.length-2]
            nextStep = pathIni[i+1]
            node[step] ?= if typeof nextStep is 'number' then [] else {}
            node = node[step]
        lastStep = pathIni[pathIni.length-1]
        { node, lastStep }

    # Set a value and save the config.
    # It creates the path, if that do not exists.
    # ```js
    # config.set('key1.subkey', 123)
    # ```
    # Is an alias to:
    # ```js
    # config.data.key1.subkey = 123
    # config.save()
    # ```
    set: (path, val)->
        { node, lastStep } = @getPath path
        if node[lastStep] is val
           Promise.resolve val
        else
           node[lastStep] = val
           @save returnVal: val

    # Get a value and create the path if it not exists.
    # ```js
    # x = config.get('key1.subkey', 123)
    # ```
    # Is an alias to:
    # ```js
    # x = config.data.key1.subkey
    # if (typeof x == 'undefined') x = 123
    # ```
    get: (path, defaultVal)->
        { node, lastStep } = @getPath path
        if node[lastStep]?
            node[lastStep]
        else
            defaultVal

    # Get a value. If it do not exists, it is seted to defaultVal
    # and the config is saved.
    getOrSet: (path, defaultVal)->
        { node, lastStep } = @getPath path
        if node[lastStep]?
            Promise.resolve node[lastStep]
        else
            node[lastStep] = defaultVal
            @save returnVal: defaultVal

loadedConfigs = {}

# The purpose of this is to centralize and simplyfy the accsess to configurations
# for Exu itself and each exu-web-app. Each app will have a direct access to a
# JSON data and the adderres to a config storage dir where it can put other files
# if needed.
configAccessor = (name)->
    if loadedConfigs[name]
        return Promise.resolve loadedConfigs[name]
    configDir = getConfigDir name
    configFile = configDir + '/data.json'
    fs.stat configDir
    .catch (err)->
        if err.code is 'ENOENT'
            fs.mkdir configDir
        else
            throw err
    .then ->
        #log 'Loading config:', configFile
        fs.readFile configFile
    .catch (err)->
        if err.code is 'ENOENT'
            log 'Create config:', configFile
            fs.writeFile configFile, '{}'
            .then -> '{}'
        else
            throw err
    .then (json)->
        config = new Config configDir, configFile, JSON.parse json
        loadedConfigs[name] = config

export { configAccessor }
