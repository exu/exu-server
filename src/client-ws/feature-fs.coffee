import path from 'path'
import run from '@aurium/run'
import { promises as fs } from 'fs'
import { log, mkID } from '../helpers'

export default (client, func)->

    client.on 'fs:readdir', func 'WS fs:readdir', ([reqID, dirPath])->
        log 'WS fs:readdir', {reqID, dirPath}
        fs.readdir dirPath, withFileTypes: true
        .then (files)->
            client.emit "fs:resp:#{reqID}", { files: files, ok: true }
        .catch (err)->
            client.emit "fs:resp:#{reqID}", { error: err, ok: false }

