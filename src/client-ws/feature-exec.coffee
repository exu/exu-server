import path from 'path'
import run from '@aurium/run'
import { log, mkID, urlDirname } from '../helpers'
import { HOST_NAME, MAIN_PORT } from '../config'

dirname = `urlDirname(import.meta.url)`

baseExecEnv = {
    SSH_ASKPASS:  path.join dirname, '..', '..', 'tools', 'ssh-askpass'
    SUDO_ASKPASS: path.join dirname, '..', '..', 'tools', 'ssh-askpass'
    EXU_MAIN_PORT: MAIN_PORT
}

procs = {}

export default (client, func)->
    client.on 'exec', func 'WS exec', ([reqID, params...])->
        log 'EXEC:', reqID, params...
        runPromise = run params...,
            cwd: '/tmp',
            addEnv: { baseExecEnv..., EXU_USER: client.webDesktop.id }
        runPromise.reqID = reqID
        procs[reqID] = runPromise
        runPromise.proc.stdout.on 'data', func (data)->
            client.emit 'exec:stdout', {reqID, data: data.toString()}
        runPromise.proc.stderr.on 'data', func (data)->
            client.emit 'exec:stderr', {reqID, data: data.toString()}
        exitStatus = 0
        runPromise
        .catch func (error)->
            exitStatus = error.statusCode
            client.emit 'exec:error', {reqID, error}
        .finally func ->
            client.emit 'exec:finished', {reqID, status: exitStatus}
            #procs = procs.filter (p)-> p isnt runPromise
            delete procs[reqID]
