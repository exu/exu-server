import socketIOBuilder from 'socket.io'
import addFeatureFS from './feature-fs'
import addFeatureApps from './feature-apps'
import addFeatureExec from './feature-exec'
import addFeatureShutdown from './feature-shutdown'
import { log, mkID, safeFuncBuilder } from '../helpers'
import { HOST_NAME, MAIN_PORT } from '../config'

io = null
clients = {}

export getClients = -> clients

extendClient = (client)->
    client.webDesktop = id: do mkID
    clients[client.webDesktop.id] = client
    client.on 'error', (err)-> client.emitError err
    client.emitError = (err)->
        if 'string' is typeof err
            error = message: err
        else
            error = {}
            error[k] = err[k] for k in [...Object.keys(err), 'message', 'stack']
        log error
        client.emit 'ERROR', error

export buildWSServer = (server)->
    io = socketIOBuilder server
    io.on 'connection', (client)->
        func = safeFuncBuilder client
        do func 'Init client socket', ->
            extendClient client
            headers = client.handshake?.headers or {}
            origin = headers.origin or headers.referer or ''
            log 'CONN Origin:', origin
            unless origin.match ///^https?://#{HOST_NAME}:#{MAIN_PORT}///
                return client.emitError 'Invalid origin'
            addFeatureFS client, func
            addFeatureApps client, func
            addFeatureExec client, func
            addFeatureShutdown client, func

export broadcast = (args...)-> io.emit args...
