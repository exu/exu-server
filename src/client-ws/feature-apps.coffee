import run from '@aurium/run'
import * as apps from '../apps'
import { promises as fs } from 'fs'
import { log, mkID } from '../helpers'

export default (client, func)->

    client.on 'apps:list', func 'WS apps:list', ([reqID, filter={}])->
        log 'WS apps:list', {reqID, filter}
        client.emit "apps:resp:#{reqID}", { apps: apps.getApps(), ok: true }

