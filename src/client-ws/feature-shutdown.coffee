import * as exuServer from '../..'
import { log, mkID, safeFuncBuilder } from '../helpers'

export default (client, func)->
    client.on 'shutdown', func 'WS shutdown', ->
        exuServer.exit exitStatus: 0, message: 'Shutdown requested by user.'

