import http from 'http'
import path from 'path'
import run from '@aurium/run'
import * as exuServer from '..'
import xdg from 'xdg-parse'
import { promises as fs } from 'fs'
import { getExuConfig, configAccessor, LANG, MAIN_PORT } from './config'
import { log, mkID, getExt, promiseChain, urlDirname } from './helpers'

#TODO: consider /usr/share/applications/defaults.list

dirname = `urlDirname(import.meta.url)`
exuConfig = null
appPaths = []
apps = {}

export getApp = (codeName)-> apps[codeName]
export getApps = -> apps

getUsedPorts = ->
    run 'netstat', '-tuln'
    .then (output)-> [
        MAIN_PORT,
        ...output.trim().split '\n'
        .filter (line)-> /^(tcp|upd).*:[0-9]+/.test line
        .map (line)-> parseInt line.split(/\s+/)[3].replace /.*:/, ''
    ]

export init = ->
    getExuConfig().getOrSet 'apps.paths', [
            path.resolve dirname + '/../web-apps'
        ]
    .then (userConfigPaths)->
        appPaths = [
            '/usr/share/applications'
            '/usr/local/share/applications'
            "#{process.env.HOME}/.local/share/applications"
            ...userConfigPaths
        ]
        updateAppList().catch (err)->
            log err
            exuServer.exit message: 'Fail to load app list.'

updateAppList = ->
    promiseChain appPaths, serchForDesktopFilesAt
    .then loadConfigForApps

serchForDesktopFilesAt = (path)->
    log 'Listing apps at', path
    fs.readdir path, withFileTypes: true
    .then (files)-> Promise.all files.map (f)->
        file = path + '/' + f.name
        if f.isDirectory()
            serchForDesktopFilesAt file
        else if getExt(file) is 'desktop'
            addApp file
    .catch (err)->
        throw err unless err.code is 'ENOENT'

pathToCodename = (filePath)->
    if /\/applications\//.test filePath
        filePath.replace /^.*\/applications\/|.desktop$/g, ''
                .replace /\/+/g, ':'
    else
        filePath.replace /^.*\/|.desktop$/g, ''

addApp = (file)->
    fs.readFile file, 'utf8'
    .then (rawData)->
        try
            codeName = pathToCodename file
            apps[codeName] = {
                ...xdg.parse rawData, LANG
                $desktopFile: file
                $codeName: codeName
            }
        catch err
            log "Fail to load #{file}:", err

loadConfigForApps = ->
    lastPort = 4000
    getUsedPorts().then (usedPorts)->
        Promise.all Object.keys(apps).map (codeName)->
            app = apps[codeName]
            configAccessor(codeName).then (config)->
                app.$config = config
                port = config.get 'port'
                unless port
                    lastPort++ while lastPort in usedPorts
                    usedPorts.push lastPort
                    config.set 'port', lastPort

