import * as exuServer from '..'
import * as clientWS from './client-ws'
import { parseParamsFromUrl, mkID } from './helpers'

export default (req, res)->
    cmd = req.url.split('?')[0].split('/')[2]
    if commands[cmd]
        commands[cmd] req, res
    else
        res.writeHead 404, 'Content-Type': 'text/html'
        res.end """
        <html>
        <head><title>(404) Command Not Found</title></head>
        <body>
            <h1>(404) Command Not Found</h1>
            <pre>The #{req.url.split('?')[0]} was not found.</pre>
        </body></html>
        """

commands = {}

commands['askpass'] = (req, res)->
    params = parseParamsFromUrl req.url
    askID = do mkID
    console.log 'API ssh-askpass', { askID, params... }
    client = clientWS.getClients()[params.user]
    client.emit 'askpass', {askID, msg: params.msg}
    client.once "askpass:#{askID}", (password)->
        console.log 'API ssh-askpass response', { askID }
        if password in [false, null]
            res.writeHead 204
            res.end null
        else
            res.writeHead 200, 'Content-Type': 'text/plain'
            res.end password

commands['shutdown'] = (req, res)->
    res.writeHead 200, 'Content-Type': 'text/plain'
    res.end 'ok'
    exuServer.exit exitStatus: 0, message: 'Shutdown requested by user.'

