import { readFileSync, statSync } from 'fs';
import { URL, pathToFileURL } from 'url';
import CoffeeScript from 'coffeescript';
//CoffeeScript.register();

const baseURL = pathToFileURL(`${process.cwd()}/`).href;

// CoffeeScript files end in .coffee, .litcoffee or .coffee.md.
const extensionsRegex = /\.coffee$|\.litcoffee$|\.coffee\.md$/;

const hasMjsKeywords = /(^|\n|;)\s*(import|export)\s/;

export function resolve(specifier, context, defaultResolve) {
  if (!/^(\.{0,2}\/|\.\.?$)/.test(specifier)) {
    return defaultResolve(specifier, context, defaultResolve);
  }

  const { parentURL = baseURL } = context;
  var url = new URL(specifier, parentURL)

  try {
    let stats = statSync(url)
    if (stats.isDirectory()) {
      try {
        // Require a dir, so try to find index.coffee
        let coffeeURL = new URL(specifier + '/index.coffee', parentURL);
        statSync(coffeeURL);
        url = coffeeURL;
      }
      catch (err) {
        try {
          // There is no index.coffee, so try to get package.json there and use its main.
          let pac = JSON.parse(readFileSync(new URL(specifier + '/package.json', parentURL)));
          url = new URL(specifier + '/' + pac.main, parentURL);
        }
        catch (err) { }
      }
    }
  }
  catch (err) {
    if (err.code === 'ENOENT') {
      try {
        // May be a extenssion-less request. So, try coffee:
        let coffeeURL = new URL(specifier + '.coffee', parentURL);
        statSync(coffeeURL);
        url = coffeeURL;
      }
      catch (err) { }
    }
  }

  // Node.js normally errors on unknown file extensions, so return a URL for
  // specifiers ending in the CoffeeScript file extensions.
  if (extensionsRegex.test(url)) {
    return { url: url.href };
  }

  // Let Node.js handle all other specifiers.
  return defaultResolve(specifier, context, defaultResolve);
}

export function getFormat(url, context, defaultGetFormat) {
  // Now that we patched resolve to let CoffeeScript URLs through, we need to
  // tell Node.js what format such URLs should be interpreted as. For the
  // purposes of this loader, all CoffeeScript URLs are ES modules.

  if (extensionsRegex.test(url)) {
    //return { format: 'module' };
    let code = readFileSync(new URL(url), 'utf8');
    if (hasMjsKeywords.test(code)) {
      console.log('>>>> IS ESM', url);
      return { format: 'module' };
    }
    else {
      console.log('>>>> IS CJS', url);
      return { format: 'commonjs' };
    }
  }

  // Let Node.js handle all other URLs.
  return defaultGetFormat(url, context, defaultGetFormat);
}

export function transformSource(source, context, defaultTransformSource) {
  const { url, format } = context;
  console.log('>>>> transformSource', url);

  if (extensionsRegex.test(url)) {
    return {
      source: CoffeeScript.compile(source, { filename: url, bare: true, inlineMap: true })
    };
  }

  // Let Node.js handle all other sources.
  return defaultTransformSource(source, context, defaultTransformSource);
}
